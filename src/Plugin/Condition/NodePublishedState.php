<?php

namespace Drupal\block_condition_published\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Node Published State' condition.
 *
 * @Condition(
 *    id = "node_published_state",
 *    label = @Translation("Node published state"),
 *    context_definitions = {
 *      "node" = @ContextDefinition(
 *        "entity:node",
 *         label = @Translation("Node")
 *       )
 *    }
 *  )
 */
class NodePublishedState extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new IsPublishedCondition instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['is_published' => 0] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['is_published'] = [
      '#title' => $this->t('Published'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['is_published'],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['is_published'] = $form_state->getValue('is_published');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $status = $this->configuration['is_published'];

    if (!empty($this->configuration['negate'])) {
      return $this->t('The published state is not @state', ['@state' => $status]);
    }
    return $this->t('The published state is @state', ['@state' => $status]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['is_published']) && !$this->isNegated()) {
      // Ignore if all checkboxes unchecked.
      return TRUE;
    }

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getContextValue('node');

    if (empty($node) || $node->isNew()) {
      // Ignore if node context isn't available.
      return !$this->isNegated();
    }

    /** @var \Drupal\node\NodeStorageInterface$node_storage */
    $node_storage = $this->entityTypeManager->getStorage('node');

    $vid = $node_storage->getLatestRevisionId($node->id());

    /** @var \Drupal\Core\Entity\RevisionableInterface $node_revision */
    $node_revision = $node_storage->loadRevision($vid);

    // @todo more info about revisions:
    //   https://www.phase2technology.com/blog/getting-correct-node
    //   https://drupal.stackexchange.com/questions/264674/what-is-the-default-revision
    return $node_revision->get('status')->value == $this->configuration['is_published'];
  }

}
