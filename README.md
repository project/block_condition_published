<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

* Version
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

VERSION
-------
Version: 1.0.0


INTRODUCTION
------------

A module that provides a condition plugin for blocks. 
Allows to display block on pages depending on publishing status of the node. 

For example, a block can be shown for editors on unpublished pages only.
Supports both classic _Published/Unpublished_ status and workflows 
(_Review, Draft..._ etc.) as well. 

In case of workflows, all states such as _Draft, Unpublished, Review..._ 
are treated as unpublished, 
state _Published_ is treated as published content.


REQUIREMENTS
------------

Drupal ^9 || ^10 || ^ 11

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------
This module doesnt not provide any global settings.

MAINTAINERS
-----------

Current maintainers:
* Nikolay Grachev (granik) - https://www.drupal.org/u/granik / developed by
* Nico Grienauer (Grienauer) - https://www.drupal.org/u/grienauer


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
